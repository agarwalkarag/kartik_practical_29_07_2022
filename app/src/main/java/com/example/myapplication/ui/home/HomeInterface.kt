package com.example.myapplication.ui.home

import androidx.lifecycle.LiveData

interface HomeInterface {

    fun onStarted()
    fun onSuccess(response: LiveData<String>)
    fun onFailure(message: String)
}