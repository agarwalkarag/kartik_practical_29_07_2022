package com.example.myapplication.ui.auth

interface AuthInterface {

    fun onStarted()
    fun onSuccess()
    fun onFailure(type:Int,message: String)
}