package com.example.myapplication.ui.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityLoginBinding
import com.example.myapplication.databinding.ActivityRegsiterBinding
import com.example.myapplication.databinding.ActivityRegsiterBindingImpl
import com.example.myapplication.utils.showToast

class RegisterActivity : AppCompatActivity(), AuthInterface {

    private lateinit var binding: ActivityRegsiterBinding
    private lateinit var viewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_regsiter)
        viewModel = ViewModelProviders.of(this)[AuthViewModel::class.java]
        binding.viewModel = viewModel
        viewModel.authInterface = this
    }

    override fun onStarted() {
    }

    override fun onStart() {
        super.onStart()

        binding.etName.addTextChangedListener {
            binding.ipName.isErrorEnabled = false
            binding.ipName.error = null
        }

        binding.etPassword.addTextChangedListener {
            binding.ipPassword.isErrorEnabled = false
            binding.ipPassword.error = null
        }

        binding.etEmail.addTextChangedListener {
            binding.ipEmail.isErrorEnabled = false
            binding.ipEmail.error = null
        }
    }

    override fun onSuccess() {
        showToast("Register Successfully")
        finish()
    }

    override fun onFailure(type: Int, message: String) {
        when (type) {
            1 -> {
                binding.ipName.isErrorEnabled = true
                binding.ipName.error = message
            }
            2 -> {
                binding.ipEmail.isErrorEnabled = true
                binding.ipEmail.error = message
            }
            3 -> {
                binding.ipPassword.isErrorEnabled = true
                binding.ipPassword.error = message
            }
        }

    }
}