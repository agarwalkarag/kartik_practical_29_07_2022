package com.example.myapplication.ui.sequence

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivitySequenceBinding

class SequenceActivity : AppCompatActivity(), SequenceInterface {

    private lateinit var binding: ActivitySequenceBinding
    private lateinit var viewModel: SequenceViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sequence)
        viewModel = ViewModelProviders.of(this).get(SequenceViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.sequenceInterface = this

        setSupportActionBar(binding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onStart() {
        super.onStart()

        binding.etRange.addTextChangedListener {
            binding.ipRange.isErrorEnabled = false
            binding.ipRange.error = null
        }

        binding.etRepeat.addTextChangedListener {
            binding.ipRepeat.isErrorEnabled = false
            binding.ipRepeat.error = null
        }
    }

    override fun onStarted() {
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home->{
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()

        finish()
    }

    override fun onSuccess(sequence: StringBuilder) {
        binding.output.text = sequence
    }

    override fun onFailure(type: Int, message: String) {
        when (type) {
            1 -> {
                binding.ipRange.isErrorEnabled = true
                binding.ipRange.error = message
            }
            2 -> {
                binding.ipRepeat.isErrorEnabled = true
                binding.ipRepeat.error = message
            }
        }
    }
}