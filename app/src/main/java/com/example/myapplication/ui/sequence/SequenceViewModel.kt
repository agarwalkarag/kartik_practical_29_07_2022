package com.example.myapplication.ui.sequence

import android.view.View
import androidx.lifecycle.ViewModel
import com.example.myapplication.utils.hideKeyboard
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.StringBuilder

class SequenceViewModel : ViewModel() {

    var repeat: String = ""
    var range: String = ""
    var sequence: String = ""
    var sequenceInterface: SequenceInterface? = null

    fun generateSequence(view: View) {

        view.context.hideKeyboard(view)
        if ( range.isBlank()) {
            sequenceInterface?.onFailure(1,"Please enter the range to be displayed")
            return
        }else if ( repeat.isBlank()) {
            sequenceInterface?.onFailure(2,"Please enter the number of repetation to be displayed")
            return
        }

        var observable: Observable<Int> = Observable
            .range(1, range.toInt())
            .repeat(repeat.toLong())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

        var sequence = StringBuilder()

        observable.subscribe(object : Observer<Int> {
            override fun onSubscribe(d: Disposable) {
                sequence = StringBuilder()
            }

            override fun onNext(int: Int) {
                sequence.append("${int} ")
                if(int==range.toInt()){
                    sequence.append("\n")
                }
            }

            override fun onError(e: Throwable) {
            }

            override fun onComplete() {
                sequenceInterface!!.onSuccess(sequence)
            }

        })


    }

}