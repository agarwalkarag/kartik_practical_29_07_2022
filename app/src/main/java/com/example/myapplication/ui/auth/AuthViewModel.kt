package com.example.myapplication.ui.auth

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModel
import com.example.myapplication.utils.hideKeyboard
import com.example.myapplication.utils.isValidEmail

class AuthViewModel : ViewModel() {

    var email: String = ""
    var password: String = ""
    var name: String = ""

    var authInterface: AuthInterface? = null

    fun onSignIN(view: View) {
        view.context.hideKeyboard(view)
        authInterface?.onStarted()
        if (!email.isValidEmail()) {
            authInterface?.onFailure(1,"Invalid Email")
            return
        } else if (password.isBlank()) {
            authInterface?.onFailure(2,"Password Mandatory")
            return
        } else {
            authInterface?.onSuccess()
        }
    }

    fun onSignUp(view: View) {
        view.context.hideKeyboard(view)
        authInterface?.onStarted()
        if (name.isBlank()) {
            authInterface?.onFailure(1,"Name Required")
            return
        } else if (!email.isValidEmail()) {
            authInterface?.onFailure(2,"Invalid Email")
            return
        } else if (password.isBlank()) {
            authInterface?.onFailure(3,"Password Mandatory")
            return
        } else {
            authInterface?.onSuccess()
        }
    }

    fun onRegister(view: View) {
        Intent(view.context, RegisterActivity::class.java).also {
            view.context.startActivity(it)
        }
    }

    fun onLoginClick(view: View) {
        Intent(view.context, LoginActivity::class.java).also {
            view.context.startActivity(it)
        }
    }
}