package com.example.myapplication.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityLoginBinding
import com.example.myapplication.ui.home.HomeActivity
import com.example.myapplication.utils.showToast

class LoginActivity : AppCompatActivity(), AuthInterface {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.authInterface = this
    }

    override fun onStart() {
        super.onStart()

        binding.etPassword.addTextChangedListener {
            binding.ipPassword.isErrorEnabled = false
            binding.ipPassword.error = null
        }

        binding.etEmail.addTextChangedListener {
            binding.ipEmail.isErrorEnabled = false
            binding.ipEmail.error = null
        }
    }

    override fun onStarted() {
    }

    override fun onSuccess() {
        showToast("Login Success")
        Intent(this, HomeActivity::class.java).also {
            startActivity(it)
        }
        finish()
    }

    override fun onFailure(type: Int, message: String) {
        when (type) {
            1 -> {
                binding.ipEmail.isErrorEnabled = true
                binding.ipEmail.error = message
            }
            2 -> {
                binding.ipPassword.isErrorEnabled = true
                binding.ipPassword.error = message
            }
        }

    }

}