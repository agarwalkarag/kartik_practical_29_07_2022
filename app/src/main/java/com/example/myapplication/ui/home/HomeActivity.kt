package com.example.myapplication.ui.home

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import com.example.myapplication.R
import com.example.myapplication.adapters.GalleryAdapter
import com.example.myapplication.databinding.ActivityHomeBinding
import com.example.myapplication.ui.sequence.SequenceActivity
import com.example.myapplication.utils.isValidJSON
import com.example.myapplication.utils.showToast
import org.json.JSONObject

class HomeActivity : AppCompatActivity(), HomeInterface {

    private lateinit var binding: ActivityHomeBinding
    private lateinit var viewModel: HomeViewModel
    var dataArrayList = arrayListOf<GalleryModel>()
    var adapterDetails: GalleryAdapter? = null
    var doubleBackToExitPressedOnce = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.homeInterface = this

        val gridLayoutManager = GridLayoutManager(this, 6)

        gridLayoutManager.spanSizeLookup = object : SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (position % 5) {
                    3 -> {
                        3
                    }

                    4 -> {
                        3
                    }

                    else -> {
                        2
                    }
                }
            }
        }

        binding.recyclerView.layoutManager = gridLayoutManager
    }

    override fun onStart() {
        super.onStart()

        binding.etCount.addTextChangedListener {
            binding.ipCount.error = null
            binding.ipCount.isErrorEnabled = false
        }

        binding.toolbar.setNavigationOnClickListener {
            Intent(this, SequenceActivity::class.java).also {
                startActivity(it)
            }
        }
    }


    override fun onStarted() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun onSuccess(response: LiveData<String>) {

        response.observe(this, Observer {
            binding.progressBar.visibility = View.GONE
            dataArrayList.clear()
            if (it.isValidJSON()) {
                val jsonBody = JSONObject(it)
                if (jsonBody.getInt("status") == 1) {
                    val jsonArray = jsonBody.getJSONArray("data")

                    var galleryModel: GalleryModel
                    for (index in 0 until jsonArray.length()) {

                        galleryModel = GalleryModel(
                            jsonArray
                                .getJSONObject(index).getString("image_url")
                        )
                        dataArrayList.add(galleryModel)
                    }

                    runOnUiThread {
                        adapterDetails = GalleryAdapter(this, dataArrayList)

                        binding.recyclerView.apply {
                            adapter = adapterDetails
                            visibility = View.VISIBLE
                        }
                    }
                } else {
                    showToast(jsonBody.getString("message"))
                    dataArrayList.clear()
                    binding.recyclerView.visibility = View.GONE
                }
            }
        })
    }

    override fun onFailure(message: String) {
        binding.progressBar.visibility = View.GONE

        binding.ipCount.error = message
        binding.ipCount.isErrorEnabled = true
    }

    override fun onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finishAffinity()
            return
        } else {
            doubleBackToExitPressedOnce = true
            showToast(resources.getString(R.string.back))
            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }
}