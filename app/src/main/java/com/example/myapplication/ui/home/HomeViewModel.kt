package com.example.myapplication.ui.home

import android.util.Log
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.example.myapplication.data.repositories.GalleryRepository
import com.example.myapplication.utils.hideKeyboard
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class HomeViewModel : ViewModel() {

    var count: String = ""
    var homeInterface: HomeInterface? = null

    fun fetchImages(view: View) {

       /* var observable: Observable<Int> = Observable
            .range(1, 10)
            .repeat(5)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

        observable.subscribe(object : Observer<Int> {
            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(int: Int) {
                Log.w("data", "$int")
            }

            override fun onError(e: Throwable) {
            }

            override fun onComplete() {
            }

        })*/

        homeInterface?.onStarted()
        if(count.isBlank()){
            homeInterface?.onFailure("Please enter the required Size")
            return
        }

        view.context.hideKeyboard(view)
        val response = GalleryRepository().fetchGallery(count.toInt())
        homeInterface?.onSuccess(response)
    }

}