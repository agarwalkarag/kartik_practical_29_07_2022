package com.example.myapplication.ui.sequence

import java.lang.StringBuilder

interface SequenceInterface {

    fun onStarted()
    fun onSuccess(sequence: StringBuilder)
    fun onFailure(type:Int, message: String)
}