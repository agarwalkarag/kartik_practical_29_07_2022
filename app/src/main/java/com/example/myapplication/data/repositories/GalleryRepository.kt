package com.example.myapplication.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myapplication.data.network.MyApi
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GalleryRepository {


    fun fetchGallery(size: Int): LiveData<String> {

        val apiResponse = MutableLiveData<String>()

        MyApi().fetchGallery(size)
            .enqueue(object : Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        apiResponse.value = response.body()?.string()
                    }else{
                        apiResponse.value = response.errorBody()?.string()
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                    apiResponse.value = t.message
                }

            })

        return  apiResponse
    }
}