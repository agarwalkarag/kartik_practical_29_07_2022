package com.example.myapplication.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.R
import com.example.myapplication.databinding.InfoRowGalleryBinding
import com.example.myapplication.ui.home.GalleryModel

class GalleryAdapter(
    var context: Context,
    var dataList: ArrayList<GalleryModel>
) :
    RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder>() {

    inner class GalleryViewHolder(binding: InfoRowGalleryBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }

    lateinit var layoutBinding: InfoRowGalleryBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        layoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.info_row_gallery, parent, false
        )
        return GalleryViewHolder(layoutBinding)
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {


        Glide.with(context)
            .load(dataList[position].url)
            .placeholder(R.drawable.app_icon)
            .into(layoutBinding.rowImg)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }


}