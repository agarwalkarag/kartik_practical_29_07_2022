package com.example.myapplication.utils

import android.app.Activity
import android.content.Context
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

fun String.isValidEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this.trim()).matches()
}

fun Context.showToast(message: String) {
    Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
}

fun String.isValidJSON(): Boolean {
    try {
        JSONArray(this)
    } catch (ex: JSONException) {
        try {
            JSONObject(this)
        } catch (ex1: JSONException) {
            return false
        }
    }
    return true
}


fun Context.hideKeyboard(view: View) {
    val inputMethodManager =
        this.getSystemService(Activity.INPUT_METHOD_SERVICE)
                as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}



